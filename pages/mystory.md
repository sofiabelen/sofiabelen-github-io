---
title: My story
layout: blog 
permalink: /mystory.html
categories: mystory
---
The thought of starting a blog has been in the back of my mind for a long time, now that I think about it. For one thing, I enjoy writing and want to share part of my life. The main goal of this blog, however, is be to connect with people with common interests, to help each other in the search and fulfillment of our passions.

I'm currently studying physics in Moscow. How and why I ended up in Russia... is also something I would like to share. I'm from Argentina, so it's probably one of the most distant places I could've chosen. Studying in a foreign country, learning a language from scratch and actually taking your classes in that language... is something I couldn't have imaged myself doing. But here I am, and I want to show you what this is really like.
