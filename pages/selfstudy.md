---
title: Why so many students hate school?
layout: blog
permalink: /selfstudy.html
categories: selfstudy
---
I know I'm not alone when I say I completely despised going to school. Why is then that we force our kids and teenagers to go through such an awful process? It's the same as being in prison, you cannot express yourself through clothing or your appearance in general, since everyone has to look the same. Although you do get to go home after so many hours, don't forget you have hours worth of homework. What if I have a hobby or different interests than what I'm being taught in school? Shouldn't I have a say in what I spend my time on? 

I'm a firm believer that you can't force someone to learn, they might memorize it for the exam, but forget it the minute they walk out the door. The only thing they accomplish, is that now most people hate learning.

Enough complaining. This blog is about re-learning to learn. I'll share what techniques I use to study and to stay motivated. I'm not an expert, just a student, but maybe someone will benefit from this. Also, I'm always looking for new ways to improve my study routine, so I'd love to hear tips from other people.
